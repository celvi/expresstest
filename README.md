# expresstest

an express app

docker login registry.gitlab.com
docker build -t registry.gitlab.com/celvi/expresstest .
docker push registry.gitlab.com/celvi/expresstest
docker pull registry.gitlab.com/celvi/expresstest

git cheat
git --version
git config --global user.name "celvi"
git config --global user.name
git config --global user.email "georgepcelvi@aol.com"
git config --global user.email
git config --global --list
user.name=celvi
user.email=georgepcelvi@aol.com
git checkout master
git checkout -b branch-name   (create a new branch)
git status
git add *
git commit -m "a commit message"
git push origin branch-name
git checkout . (delete all changes except unstaged work)
git clean -f (delete all changes and untracked files)

branch checkout (stage change) and merge to master
git checkout branch-name
git merge master

branch checkout master (stage change) and merge to branch
git checkout master
git merge branch-name
git push
git branch -d branch-name (delete local branch)

git commit -u (update / deleted files)


npm start
npm run dev

curl http://localhost:8080/

mocha 

docker-compose build
docker-compose up

Creating network "expresstest_backend" with driver "bridge"
Creating expresstest_api_1     ... done
Creating expresstest_web-cli_1 ... done
Attaching to expresstest_web-cli_1, expresstest_api_1
api_1      | 
api_1      | > expresstest@0.0.0 start /usr/src/app
api_1      | > node ./bin/www
api_1      | 
expresstest_web-cli_1 exited with code 0
api_1      | server listening on port: 3000
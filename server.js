require('dotenv').config()
//Load express module with `require` directive
var express = require('express')
var app = express()

//Define request response in root URL (/)
app.get('/', function (req, res) {
  res.status(200).send(process.env.TITLE);
})

//Launch listening server on port configured
var server = app.listen(process.env.PORT, function () {
  console.log('App listening on port->',process.env.PORT)
})
module.exports = server;